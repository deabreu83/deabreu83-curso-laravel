<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'name', 'lastname', 'email', 'address', 'phone'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
