<?php

use Illuminate\Database\Seeder;
//use App\Contact;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*User::create([
            'name' => 'Carla Ramirez',
            'email' => 'carla@gmail.com@gmail.com',
            'password' => bcrypt('123456')
        ]);*/
        factory(App\Contact::class, 10)->create();
    }
}
